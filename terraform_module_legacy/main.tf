resource "random_password" "root_password" {
  length  = 32
  special = false
  lifecycle {
    ignore_changes = all
  }
}

locals {
  name_with_chart = "${var.chart_name}-mariadb"
}

resource "helm_release" "mariadb" {
  chart           = "mariadb"
  repository      = "https://charts.bitnami.com/bitnami"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "primary.resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "primary.resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "primary.resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "primary.resources.requests.memory"
      value = var.requests_memory
    }
  }

  set {
    type  = "string"
    name  = "auth.rootPassword"
    value = random_password.root_password.result
  }

  dynamic "set" {
    for_each = var.mariadb_auth_database == null ? [] : [var.mariadb_auth_database]
    content {
      name  = "auth.database"
      value = var.mariadb_auth_database
    }
  }

  dynamic "set" {
    for_each = var.mariadb_auth_username == null ? [] : [var.mariadb_auth_username]
    content {
      name  = "auth.username"
      value = var.mariadb_auth_username
    }
  }

  dynamic "set" {
    for_each = var.mariadb_auth_password == null ? [] : [var.mariadb_auth_password]
    content {
      name  = "auth.password"
      value = var.mariadb_auth_password
    }
  }

  set {
    name  = "fullnameOverride"
    value = local.name_with_chart
  }

  set {
    name  = "persistence.size"
    value = var.mariadb_persistence_size
  }
}
