variable "mariadb_auth_database" {
  type    = string
  default = null
}

variable "mariadb_auth_username" {
  type    = string
  default = null
}

variable "mariadb_auth_password" {
  type    = string
  default = null
}

variable "mariadb_persistence_size" {
  type    = string
  default = "8Gi"
}
