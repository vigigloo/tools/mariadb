output "root_password" {
  value = random_password.root_password.result
}

output "host" {
  value = "${local.name_with_chart}.${var.namespace}.svc.cluster.local"
}
